<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function userListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile');
        $this->db->from('tbl_users as BaseTbl');
       if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        
        $query = $this->db->get();
        
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function userListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile');
        $this->db->from('tbl_users as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
   
    /**
     * This function is used to check whether email id is already exist or not
     * @param {string} $email : This is email id
     * @param {number} $userId : This is user id
     * @return {mixed} $result : This is searched result
     */
    function checkEmailExists($email, $userId = 0)
    {
        $this->db->select("email");
        $this->db->from("tbl_users");
        $this->db->where("email", $email);   
        $this->db->where("isDeleted", 0);
        if($userId != 0){
            $this->db->where("userId !=", $userId);
        }
        $query = $this->db->get();

        return $query->result();
    }
    
    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewUser($userInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_users', $userInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfo($userId)
    {
        $this->db->select('userId, name, email, mobile');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
		$this->db->where('userId', $userId);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editUser($userInfo, $userId)
    {
        $this->db->where('userId', $userId);
        $this->db->update('tbl_users', $userInfo);
        
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser($userId, $userInfo)
    {
        $this->db->where('userId', $userId);
        $this->db->update('tbl_users', $userInfo);
        
        return $this->db->affected_rows();
    }


    /**
     * This function is used to match users password for change password
     * @param number $userId : This is user id
     */
    function matchOldPassword($userId, $oldPassword)
    {
        $this->db->select('userId, password');
        $this->db->where('userId', $userId);        
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('tbl_users');
        
        $user = $query->result();

        if(!empty($user)){
            if(verifyHashedPassword($oldPassword, $user[0]->password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }
    
    /**
     * This function is used to change users password
     * @param number $userId : This is user id
     * @param array $userInfo : This is user updation info
     */
    function changePassword($userId, $userInfo)
    {
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', $userInfo);
        
        return $this->db->affected_rows();
    }
	
	function isSchemaExists()
	{
		$db =$this->db->database;;
		$sql = "SELECT * 
			FROM information_schema.tables
			WHERE table_name = 'v_users' and table_schema = '$db'
			LIMIT 1;";
		$result = $this->db->query($sql)->result_array();

		return count($result);
	}
	
	function createSchema()
	{
		$sql = "create view v_users as select a.*, nm_jbtn, nama, trim(cast(aes_decrypt(`a`.`id_user`,'nur') as char(700))) 
		AS `id_usr`, trim(cast(aes_decrypt(`a`.`password`,'windi') as char(700))) AS `pass` from `user` a join petugas b on trim(cast(aes_decrypt(`a`.`id_user`,'nur') as char(700)))  = b.nip left join jabatan c on c.kd_jbtn = b.kd_jbtn
		union
		select a.*, nm_sps, nm_dokter, trim(cast(aes_decrypt(`a`.`id_user`,'nur') as char(700))) 
		AS `id_usr`, trim(cast(aes_decrypt(`a`.`password`,'windi') as char(700))) AS `pass` from `user` a join dokter b on trim(cast(aes_decrypt(`a`.`id_user`,'nur') as char(700)))  = b.kd_dokter left join spesialis c on c.kd_sps = b.kd_sps
		";
		$this->db->query($sql);
	}
	
	function updateSchema()
	{
		$sql = "alter view v_users as select a.*, nm_jbtn, nama, trim(cast(aes_decrypt(`a`.`id_user`,'nur') as char(700))) 
		AS `id_usr`, trim(cast(aes_decrypt(`a`.`password`,'windi') as char(700))) AS `pass` from `user` a join petugas b on trim(cast(aes_decrypt(`a`.`id_user`,'nur') as char(700)))  = b.nip left join jabatan c on c.kd_jbtn = b.kd_jbtn
		union
		select a.*, nm_sps, nm_dokter, trim(cast(aes_decrypt(`a`.`id_user`,'nur') as char(700))) 
		AS `id_usr`, trim(cast(aes_decrypt(`a`.`password`,'windi') as char(700))) AS `pass` from `user` a join dokter b on trim(cast(aes_decrypt(`a`.`id_user`,'nur') as char(700)))  = b.kd_dokter left join spesialis c on c.kd_sps = b.kd_sps
		";
		$this->db->query($sql);
	}
}

  
<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Setting_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function settingUserCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('v_users');
        
        $query = $this->db->get();
        
        return count($query->result());
    }
	
	function settingPetugasCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('petugas');
		$this->db->join('jabatan', 'petugas.kd_jbtn = jabatan.kd_jbtn');
        
        $query = $this->db->get();
        
        return count($query->result());
    }
	
	function settingDokterCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('dokter');
		$this->db->join('spesialis', 'dokter.kd_sps = spesialis.kd_sps');
        
        $query = $this->db->get();
        
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function settingListing($page, $segment)
    {
        $this->db->select('*');
        $this->db->from('v_users');
        
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
	
	function settingPetugasListing($page, $segment)
    {
        $this->db->select('*');
        $this->db->from('petugas');
		$this->db->join('jabatan', 'petugas.kd_jbtn = jabatan.kd_jbtn');
        
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
	
	function settingDokterListing($page, $segment)
    {
        $this->db->select('*');
        $this->db->from('dokter');
		$this->db->join('spesialis', 'dokter.kd_sps = spesialis.kd_sps');
        
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
	
	function allUser()
	{
		$this->db->select('*');
        $this->db->from('v_users');
                
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
	}
    
	function encrypt($input, $hash)
	{
		$sql = "select AES_ENCRYPT('$input', '$hash') as result";
		
		return $this->db->query($sql)->row()->result;
	}
	
	function decrypt($input, $hash)
	{
		$sql = "select AES_DECRYPT('$input', '$hash') as result";
		
		return $this->db->query($sql)->row()->result;
	}
   
    function settingColumns()
	{
		$sql = "SELECT COLUMN_NAME, COLUMN_COMMENT FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" . $this->db->database . "' AND TABLE_NAME = 'user' and ORDINAL_POSITION > 2 order by ORDINAL_POSITION;";

		$result= $this->db->query($sql)->result();
		return $result;
	}
	
	function fieldUser($id_user)
	{		
		$sql = "SELECT * from v_users where id_usr = '$id_user'";
		$result= $this->db->query($sql)->result_array();
		return $result;
	}
	
	function createUser($nip, $keyUser, $keyPwd, $copy)	
    {
	
		if($nip != $copy)
		{
			$sql = "delete from  user where AES_DECRYPT(id_user, '$keyUser') = '$nip' ";
			
			$this->db->query($sql);
			
			
			
			if($copy) {
				$columns = $this->settingColumns();
				$strcol = "";
				foreach($columns as $c)
				{			
					$strcol.=$c->COLUMN_NAME . ",";
				}
				$strcol = trim($strcol, ",");	
				$sql = "insert into user (id_user, password, $strcol)
						select AES_ENCRYPT('$nip', '$keyUser'), 
						AES_ENCRYPT('$nip', '$keyPwd'), $strcol 
						from user where AES_DECRYPT(id_user, '$keyUser') = '$copy'";
				$this->db->query($sql);
			}
			else{
				$columns = $this->settingColumns();
				$strcol = "";
				$strcolval = "";
				foreach($columns as $c)
				{			
					$strcol.=$c->COLUMN_NAME . ",";
					$strcolval.="false,";
				}
				$strcol = trim($strcol, ",");	
				$strcolval = trim($strcolval, ",");	
				$sql = "insert into user (id_user, password, $strcol)
						select AES_ENCRYPT('$nip', '$keyUser'), 
						AES_ENCRYPT('$nip', '$keyPwd'), $strcolval";
						
				$this->db->query($sql);
				
			
			}
			
			
			
		}
        return TRUE;
    }
	
	function editUser($userInfo, $userId, $key)	
    {
		
        $this->db->where("AES_DECRYPT(id_user, '$key') = '$userId'");
        $this->db->update('user', $userInfo);
        
        return TRUE;
    }
	
	function checkDuplicate($idUser, $key)
    {
		
        $sql = "SELECT * from user where AES_ENCRYPT(id_user, '$key') = '$idUser'";
		
		$result= $this->db->query($sql)->result();
		
      
        return $result;
    }
	
	function editPassword($password, $userId, $keyUsr, $keyPwd)
    {
		
		$sql = "update user set password = AES_ENCRYPT('$password', '$keyPwd') where AES_DECRYPT(id_user, '$keyUsr') = '$userId'";
		
		$this->db->query($sql);
    }
	
	function copyPrivileges($from, $to, $keyUsr)
    {
		if($from != $to) {
			$columns = $this->settingColumns();
			
			$set = "";
			foreach($columns as $c)
			{
				$col = $c->COLUMN_NAME;
				$set .= "m1." . $col . " = (select * from (select $col from user as x where AES_DECRYPT(id_user, '$keyUsr') = '$from') as x) ,"; 
			}
			$set = trim($set, ",");
			
			$sql = "update user as m1
				set $set
				where AES_DECRYPT(m1.id_user, '$keyUsr') = '$to'";
			
			$this->db->query($sql);
		}
    }

}

  
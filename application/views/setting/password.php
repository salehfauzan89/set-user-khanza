


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> User Management
        <small>Edit Password User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row ">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Password Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
					
                    
                    <form role="form" id="addUser" action="<?php echo base_url() ?>setting/storePassword" method="post" role="form">
					<div id="temp"></div>
					
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">New Password</label>
                                        <input type="password" class="form-control" autofocus required name="password"/>
                                    </div>
                                    
                                </div>
                               
                            </div>
							
							<div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                           
                        </div>
							
						<div class="row">
								<div class="col-md-12"><table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>User</th>
												<th>Nama</th>
												<th>Password</th>
												<th>Jabatan</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach($users as $f) { 
												
											?>
												<tr>
													<td><input class="checkboxs" data-col="<?php echo $f->id_usr;?>" value="<?php echo $f->id_usr;?>" type="checkbox" id="user_<?php echo $f->id_usr;?>"/> <?php echo $f->id_usr;?></td>
													<tD><?php echo $f->nama;?></td>
													<tD><?php echo $f->pass;?></td>
													<tD><?php echo $f->nm_jbtn;?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
                           
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                           
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>

<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<script>
	$(document).ready(function()
	{
			$(".checkboxs").click(function()
			{
				var checked = $(this).is(":checked");
				var val = $(this).val();
				var col = $(this).data("col");
				
				if(checked)
				{
				
					$("#temp").append("<input type='hidden' class='" + col + "' name='user_"  + col + "' value='" + val + "'/>");
				}
				else{
					$("#temp").find("." + col).remove();
				}
			});
	});
</script>